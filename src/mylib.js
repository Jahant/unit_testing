/** Basic arithmetic operations */
const myLib = {
    /** Multiline arrow function. */
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    subtract: (a, b) => {
        return a - b;
    },
    /** Gives error if divided by 0. */
    divide: (dividend, divisor) => {
        if(divisor == 0){
            return "Can't divide by 0"
        }
        else{
            return dividend / divisor
        }
    } ,
    /** Regular function */
    multiply: function(a, b) {
        return a * b;
    } 
};

module.exports = myLib;