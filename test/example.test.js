const expect = require('chai').expect;
const myLib = require('../src/mylib');

describe("Our first unit tests", () => {
    before(() => {
        // initialization
        // create objects
        console.log("Initialising tests.");
    });
    it("Can add 1 and 2 together", () => {
        expect(myLib.add(1,2)).equal(3, "1 + 2 is not 3, for some reason?")
    });
    it("Can multiply 7 and 8 together", () => {
        expect(myLib.multiply(7,8)).equal(56, "7 * 8 is not 56, for some reason?")
    });
    it("Can subtract 7 from 8", () => {
        expect(myLib.subtract(8,7)).equal(1, "8 - 7 is not 1, for some reason?")
    });
    it("Can divide 48 by 4", () => {
        expect(myLib.divide(48,4)).equal(12, "48 / 4 is not 12, for some reason?")
    });
    it("Dividing by 0 gives error", () => {
        expect(myLib.divide(1,0)).equal("Can't divide by 0", "1 / 0 is not error, for some reason?")
    });
    after(() => {
        // cleanup
        // for example: shutdown express server
        console.log("Testing completed!");
    })
})